/*
 * TextProfilePrinter.java
 *
 * Prints a text single dive table onto the Text Area
 *
 *   This program is part of MV-Plan
 *   Copywrite 2006 Guy Wittig
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package mvplan.dive.printer;

import java.math.BigDecimal;
import java.util.List;


import mvplan.dive.Profile;
import mvplan.dive.printer.ProfilePrinter;

import mvplan.gas.Gas;
import mvplan.main.IMvplan;
import mvplan.main.MvplanInstance;
import mvplan.segments.SegmentAbstract;
//import java.util.MissingResourceException;

public class TextProfilePrinter extends ProfilePrinter <StringBuffer>{
    private IMvplan mvplan = MvplanInstance.getMvplan();
   
    private StringBuffer textArea;
    private Profile profile;
    //private boolean showStopTime = MvplanInstance.getPrefs().isShowStopTime();
    private String disclaimer;
    private List<Gas> knownGases;
    
    /** Creates a new instance of ProfilePrinter */
    public TextProfilePrinter(Profile p, StringBuffer text, List<Gas> knownGases) {
        super(p, text, knownGases);
        this.profile=p;
        this.textArea=text;
        this.knownGases = knownGases;
        disclaimer = mvplan.getResource("mvplan.disclaimer.text");            
    }
    /** Creates a new instance of ProfilePrinter */
    public TextProfilePrinter(Profile p, StringBuffer text) {
        super(p, text, p.getGases());
        this.profile=p;
        this.textArea=text;
        this.knownGases = p.getGases();
        disclaimer = mvplan.getResource("mvplan.disclaimer.text");            
    }
    /* 
     * Prints the dive table
     */
    public StringBuffer print() {
        if(MvplanInstance.getPrefs().getOutputStyle()==MvplanInstance.getPrefs().BRIEF)
            doPrintShortTable();
        else
            doPrintExtendedTable();
        return textArea;
    }

    /** 
     * Prints an extended dive table to the textArea 
     */
    private void doPrintExtendedTable() {
        String lenghtUnits = MvplanInstance.getPrefs().getDepthShortString();
        String Deco;
        double startDTR=0;
        double endDTR=0;

        if (MvplanInstance.getPrefs().getOcDeco()) Deco = "OC/Bailout";
        else Deco = "Closed Circuit";
        if(profile.getIsRepetitiveDive()) {
            // Print repetitive dive heading
            textArea.append(mvplan.getResource("mvplan.gui.text.ProfilePrinter.repetitiveDive.text")+" "+mvplan.getAppName()+" "+
                    mvplan.getResource("mvplan.gui.text.ProfilePrinter.surfaceInterval.text")+profile.getSurfaceInterval()+" "+
                    mvplan.getResource("mvplan.minutes.shortText"));
        } else {
            textArea.append(mvplan.getAppName());
        }

        // Print settings heading
        textArea.append("<ul>");
        textArea.append("<li>" + mvplan.getResource("mvplan.gui.text.ProfilePrinter.settings.text")+profile.getModel().getModelName()+" GF: "+
                        (int)Math.round(MvplanInstance.getPrefs().getGfLow()*100.)+"-"+(int)Math.round(MvplanInstance.getPrefs().getGfHigh()*100.) + "</li>");
        textArea.append("<li>"+ mvplan.getResource("mvplan.gui.text.ProfilePrinter.factors.text") + MvplanInstance.getPrefs().getFactorComp() + "/"+ MvplanInstance.getPrefs().getFactorDecomp() + "</li>");
        if (MvplanInstance.getPrefs().getGfMultilevelMode()) {
            textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.ProfilePrinter.multilevel.text") + "</li>");
        }
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.ProfilePrinter.pph2o.text")+": "+
                        MvplanInstance.getPrefs().getPH2O()+" "+MvplanInstance.getPrefs().getDepthShortString()+
                        mvplan.getResource("mvplan.gui.text.ProfilePrinter.seaWater.shortText") + "</li>");
        textArea.append("<li>" + Deco + "</li>");
        printAltitude();
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.desRate.text") + " " + (int)MvplanInstance.getPrefs().getDescentRate() + " " + lenghtUnits+"/min" + "</li>");
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.ascRate.text") + " " + (int)MvplanInstance.getPrefs().getAscentRate() + " " + lenghtUnits+"/min" + "</li>");
        textArea.append("</ul>");

        //table détaillée
        textArea.append("<div style='overflow-x: auto;'>");
        textArea.append("<table class='table100'>");
        //Seq
        textArea.append("<tr><th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.seq.text") + "</th>");
        //Prof
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.depth.text") + "</th>");
        //Durée
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.time.text") + "</th>");
        //TpsTotal
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.totalTime.text") + "</th>");
        //Gaz
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.gas.text") + "</th>");
        //SP
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.setpoint.text") + "</th>");
        //END
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.END.text") + "</th>");
        //M-Value
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.mvalue.text") + "</th>");
        //GF
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.gf.text") + "</th></tr>");


        for(SegmentAbstract s : profile.getProfile()) {
            if(s.getType() == SegmentAbstract.CONST){
                 startDTR=s.getRunTime();
             }
             if(s.getType() == SegmentAbstract.DECO){
                 endDTR=s.getRunTime();
             }

            textArea.append(s.toStringLong());
        }

        textArea.append("</table>");
        textArea.append("</div>");

        textArea.append("<ul>");
        textArea.append(String.format("<li>%1$s : %2$.0f min</li>",mvplan.getResource("mvplan.gui.text.tts.text"),endDTR-startDTR + 1));
        doGasUsage();        
        textArea.append("</ul>");

        textArea.append("<p>" + disclaimer+"</p>");
    }
    
    /* 
     * Prints gas usage table 
     */
    private void doGasUsage() {
        //TODO - use String formatter for these so as to display localisations properly        
        // Display gas usage
        // GW - Modified Mar-2009 to display all knaown gases with volumes > 0 so as to pick up open circuit bottom gas
        List<Gas> gases=knownGases; //profile.getGases();
        String volumeUnits = MvplanInstance.getPrefs().getVolumeShortString();

        // Gas usage heading
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.ProfilePrinter.gasEstimate.text")+": "+
                (int)MvplanInstance.getPrefs().getDiveRMV()+" "+volumeUnits+"/"+ mvplan.getResource("mvplan.minutes.shortText")+"</li>"+
                "<li>" + mvplan.getResource("mvplan.gui.text.ProfilePrinter.decoRmv.text")+": "+
                (int)MvplanInstance.getPrefs().getDecoRMV()+" "+volumeUnits+"/"+ mvplan.getResource("mvplan.minutes.shortText")+"</li>");
        for (Gas g : gases){
            if(g.getDiveVolume() > 0.0d){
                textArea.append("<li>Dive : " + g+" : "+ roundDouble(1, g.getDiveVolume())+volumeUnits+"</li>");
            }

            if(g.getVolume()> 0.0d)
                textArea.append("<li>"+g+": "+ (int)roundDouble(0, g.getVolume())+" "+volumeUnits+"</li>");
        }
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.ProfilePrinter.oxygenToxcicity.text")+" "+
                        (int)profile.getModel().getOxTox().getOtu()+ " "+mvplan.getResource("mvplan.gui.text.ProfilePrinter.cns.text")+
                        ": "+(int)(profile.getModel().getOxTox().getCns()*100.)+"%"+"</li>");
        if (profile.getModel().getOxTox().getMaxOx() > MvplanInstance.getPrefs().getMaxPO2() )
            textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.ProfilePrinter.warningPpO2.text")+": "+ ((int)(profile.getModel().getOxTox().getMaxOx()*100)/100.0)+
                    " "+mvplan.getResource("mvplan.gui.text.ProfilePrinter.atmCnsEstimate.text")+"</li>");
    }
    
    /** 
     * Prints a short text dive table to the textArea 
     */
    private void doPrintShortTable() {              

        int segTimeMins, segTimeSeconds;
        String lenghtUnits = MvplanInstance.getPrefs().getDepthShortString();
        String Deco;
        double startDTR=0;
        double endDTR=0;

        if (MvplanInstance.getPrefs().getOcDeco()) Deco = "OC/Bailout";
        else Deco = "CC";

        if(profile.getIsRepetitiveDive()) {
            // Print repetitive dive heading
            textArea.append(mvplan.getResource("mvplan.gui.text.ProfilePrinter.repetitiveDive.text")+" "+mvplan.getAppName()+" "+
                    mvplan.getResource("mvplan.gui.text.ProfilePrinter.surfaceInterval.text")+profile.getSurfaceInterval()+" "+
                    mvplan.getResource("mvplan.minutes.shortText"));
        } else {
            textArea.append(mvplan.getAppName());
        };

        // Print settings heading
        textArea.append("<ul><li>" + mvplan.getResource("mvplan.gui.text.ProfilePrinter.settings.text")+profile.getModel().getModelName()+" GF: "+
                        (int)Math.round(MvplanInstance.getPrefs().getGfLow()*100.)+"-"+(int)Math.round(MvplanInstance.getPrefs().getGfHigh()*100.) + "</li>");
        if( MvplanInstance.getPrefs().isUsingFactors()) {
            textArea.append("<li>" + mvplan.getResource("mvplan.gui.text.ProfilePrinter.factors.text") + MvplanInstance.getPrefs().getFactorComp() + "/" + MvplanInstance.getPrefs().getFactorDecomp() + "</li>");
        }
        if (MvplanInstance.getPrefs().getGfMultilevelMode()) {
            textArea.append("<li>" + mvplan.getResource("mvplan.gui.text.ProfilePrinter.multilevel.text") + "</li>");
        }
        textArea.append("<li>"+Deco+ "</li>");
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.altitude.text") + " " + (int)MvplanInstance.getPrefs().getAltitude() + " " + lenghtUnits + "</li>");
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.desRate.text") + " " + (int)MvplanInstance.getPrefs().getDescentRate() + " " + lenghtUnits+"/min" + "</li>");
        textArea.append("<li>"+mvplan.getResource("mvplan.gui.text.ascRate.text") + " " + (int)MvplanInstance.getPrefs().getAscentRate() + " " + lenghtUnits+"/min" + "</li>");
//        printAltitude();
        boolean ocMode = profile.getPrefs().isOcMode();
        textArea.append("</ul>");

        textArea.append("<table class='table100'>");
        //Seq
        textArea.append("<tr><th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.seq.text") + "</th>");
        //Prof
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.depth.text") + "</th>");
        //Durée
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.time.text") + "</th>");
        //TpsTotal
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.totalTime.text") + "</th>");
        //Gaz
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.gas.text") + "</th>");
        //SP
        textArea.append("<th>" + mvplan.getResource("mvplan.gui.MainFrame.diveTable.setpoint.text") + "</th></tr>");

        double prev_run = 0.0;
        double tm = 0.0;
        for (SegmentAbstract s: profile.getProfile()){
			segTimeMins = (int) s.getTime();
			segTimeSeconds = (int) ((s.getTime() - (double) segTimeMins) * 60.0);
            if(s.getType() == SegmentAbstract.CONST){
                startDTR=s.getRunTime();
            }
            if(s.getType() == SegmentAbstract.DECO){
                endDTR=s.getRunTime();
            }

            tm = s.getRunTime() - prev_run;
            if ((s.getDepth() - (int) s.getDepth()) > 0) { // Do we have
															// non-integer depth ?

				if (ocMode) {
					textArea.append(String.format(
							"<tr><td>%1$s</td><td>%2$03.1f</td><td>%3$3.0f:00</td><td>%4$3.0f:00</td><td>%5$5s</td></tr>", //%3$02d:%4$02d
							s.getTypeString(), s.getDepth(), tm,
//                            segTimeMins, segTimeSeconds,
                            s.getRunTime(), s.getGas()
									.getShortName()));
				} else {
					textArea.append(String
							.format("<tr><td>%1$s</td><td>%2$03.1f</td><td>%3$3.0f:00</td><td>%4$3.0f:00</td><td>%5$5s</td><td>%6$3.1f</td></tr>", //%3$02d:%4$02d
									s.getTypeString(), s.getDepth(), tm,
//									segTimeMins, segTimeSeconds,
									s.getRunTime(), s.getGas().getShortName(),
									s.getSetpoint()));
				}
			} else {
				if (ocMode) {
					textArea.append(String
							.format("<tr><td>%1$s</td><td>%2$03.0f</td><td>%3$3.0f:00</td><td>%4$3.0f:00</td><td>%5$5s</td></tr>", //%3$02d:%4$02d
									s.getTypeString(), s.getDepth(), tm,
//									segTimeMins, segTimeSeconds,
									s.getRunTime(), s.getGas().getShortName()));
				} else {
					textArea.append(String
							.format("<tr><td>%1$s</td><td>%2$3.0f</td><td>%3$3.0f:00</td><td>%4$3.0f:00</td><td>%5$5s</td><td>%6$3.1f</td></tr>", //%3$02d:%4$02d
									s.getTypeString(), s.getDepth(), tm,
//									segTimeMins, segTimeSeconds,
									s.getRunTime(), s.getGas().getShortName(),
									s.getSetpoint()));
				}
			}
            prev_run = s.getRunTime();
		}

        textArea.append("</table>");

        textArea.append("<ul>");
        textArea.append(String.format("<li>%1$s : %2$.0f min</li>",mvplan.getResource("mvplan.gui.text.tts.text"),endDTR-startDTR + 1));
        doGasUsage();        
        textArea.append("</ul>");
        textArea.append("<p>" + disclaimer+"</p>");
    }
    
    /* 
     * Print altitude message 
     */
    private void printAltitude() {
        // Is this an altitude dive ?
//        if(MvplanInstance.getPrefs().getAltitude()>0.0) {
            textArea.append("<li>" + String.format("%1$s %2$4.0f%6$s (%4$1.2f%3$s) %5$s",
                    mvplan.getResource("mvplan.gui.text.altitude.text"),
                    MvplanInstance.getPrefs().getAltitude(), 
                    mvplan.getResource("mvplan.bar.text"),
                    MvplanInstance.getPrefs().getPAmb()/MvplanInstance.getPrefs().getPConversion(),
                    mvplan.getResource("mvplan.gui.text.altitudeCalibration.text"),
                    MvplanInstance.getPrefs().getDepthShortString() ) +"</li>");
 //       }
    }
    
     /* 
     * Rounds double values
     */
    private double roundDouble(int precision, double d){        
        int decimalPlace = precision;
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();                
    }

    
}
