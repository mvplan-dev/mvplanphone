/*
 * MVPlanInstance.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mvplan.main;

import mvplan.prefs.Prefs;
import mvplan.util.Version;

/**
 *
 * @author Maciej Kaniewski
 */
public class MvplanInstance {
	public static final String NAME = "MV-Plan";    // Application name
	public static final int MAJOR_VERSION = 1; // Application version codes
	public static final int MINOR_VERSION = 8;
	public static final int PATCH_VERSION = 0;
	public static final String VERSION_STATUS = "RELEASED"; // Application status
	public static final String BUILD_DATE = "18-03-2020"; // Application
															// release date
	public static Version mvplanVersion;    // App version. See Version Class
	private static mvplan.main.IMvplan mvplan;

	static{
		 mvplanVersion=new Version(MAJOR_VERSION,MINOR_VERSION,PATCH_VERSION,VERSION_STATUS,BUILD_DATE);
	}

	public static Prefs getPrefs() {
		if (mvplan != null) {
			return mvplan.getPrefs();
		}
		return null;
	}
	public static void setPrefs(Prefs p) {
		if (mvplan != null) {
			mvplan.setPrefs(p);
		}
	}

	public static mvplan.main.IMvplan getMvplan() {
		return mvplan;
	}

	public static void setMvplan(mvplan.main.IMvplan m) {
		mvplan = m;
		mvplan.init();
	}

	public static Version getVersion() {
		return mvplanVersion;
	}

}
