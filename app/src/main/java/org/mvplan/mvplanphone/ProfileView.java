/*
 * ProfileView.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone;

import mvplan.dive.Profile;
import mvplan.dive.printer.TextProfilePrinter;
import mvplan.main.MvplanInstance;
import android.os.Bundle;
import android.webkit.WebView;

public class ProfileView extends mvPlanPhoneActivity {

	WebView view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_text_view);
		view = (WebView) findViewById(R.id.profile_text_view_text);

	}

	public void calculate() {
		Profile p = new Profile(MvplanInstance.getPrefs().getPrefSegments(),
				MvplanInstance.getPrefs().getPrefGases(), null);
		// m.printModel();
		int returnCode = p.doDive();
		Profile currentProfile;
		switch (returnCode) {
		case Profile.SUCCESS:
			currentProfile = p; // Save as current profile
			p.doGasCalcs(); // Calculate gases
			// Save Model
			// Model currentModel = p.getModel();
			// System.out.println("Dive Metadata:"+currentModel.getMetaData());
			StringBuffer b = new StringBuffer();
			//textArea.append("<style>tr:nth-child(odd){ background-color:#eee; }\n" + "tr:nth-child(even)    { background-color:#fff; }</style>\n");
			b.append("<html><head>");
			b.append("<style type='text/css'>");
			b.append(".table100 th { color:%23fff; line-height:1.4; background-color:%23000000; padding: 10px; }");
			b.append(".table100 td { padding: 5px 10px 5px 10px; color:black; }");
			b.append(".table100 tr:nth-child(even) { background-color:%23aeaeae} ");
			b.append(".table100 { border-collapse: collapse; border-radius:10px; overflow:hidden; box-shadow:0 0 40px 0 rgba(0,0,0,.15);");
			b.append("</style>");
			b.append("</head>");
			b.append("<body>");

			new TextProfilePrinter(currentProfile, b).print();
            //view.setTextAppearance(this, R.style.ResultingProfile);
			//view.setText(b);
			b.append("</body>");
			b.append("</html>");
			view.loadData(b.toString(), "text/html", "utf-8");
			break;

		case Profile.CEILING_VIOLATION:
			//view.setText(MvplanInstance.getMvplan()
			//		.getResource("mvplan.gui.MainFrame.ceilingViolation.text"));
			view.loadData(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.ceilingViolation.text").toString(), "text/html", null);
			break;

		case Profile.NOTHING_TO_PROCESS:
			//view.setText(MvplanInstance.getMvplan()
			//		.getResource("mvplan.gui.MainFrame.noSegments.text"));
			view.loadData(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.noSegments.text").toString(), "text/html", null);
			break;
		case Profile.PROCESSING_ERROR:
			//view.setText(MvplanInstance.getMvplan()
			//		.getResource("mvplan.gui.MainFrame.processingError.text"));
			view.loadData(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.processingError.text").toString(), "text/html", null);
		case Profile.INFINITE_DECO:
			//view.setText(MvplanInstance.getMvplan()
			//		.getResource("mvplan.gui.MainFrame.decoNotPossible.text"));
			view.loadData(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.decoNotPossible.text").toString(), "text/html", null);

		default:
			//view.setText("Can not calculate");
			view.loadData("Can not calculate", "text/html", null);
			break;

		}
	}
	
}
