/*
 * Prefs.java
 *
 *  Maintains application preferences. Is serialised to XML for persistance.
 *
 *  This is a Singleton object in that only one should be instatiated. However this is not
 *  enforced via the Singleton Pattern.
 *
 *   @author Guy Wittig
 *   @version 28-Jun-2006
 *
 *   This program is part of MV-Plan
 *   Copywrite 2006 Guy Wittig
 *
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */


package org.mvplan.mvplanphone;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import mvplan.gas.Gas;
import mvplan.main.MvplanInstance;

import org.mvplan.mvplanphone.gui.GasDialog;
import org.mvplan.mvplanphone.gui.GasDialogCallback;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;

public class GasList extends mvPlanPhoneActivity {
	Button add;
	ListView list;
	GasListAdaptor a;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gas_list_view);

		list = (ListView) findViewById(R.id.gas_list_list);
		add = (Button) findViewById(R.id.gas_list_add);
		add.setOnClickListener(addButtonListener);

	}

	protected void onResume() {
		super.onResume();
		a = new GasListAdaptor(this.getApplicationContext(),
				R.layout.gas_list_label, R.id.gas_list_label_text,
				MvplanInstance.getPrefs().getPrefGases());
		list.setAdapter(a);
	};

	android.view.View.OnClickListener addButtonListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			new GasDialog(GasList.this, addCallback).show();
		}
	};

	GasDialogCallback addCallback = new GasDialogCallback() {
		public void notify(Gas g) {
			MvplanInstance.getPrefs().getPrefGases().add(g);
			Collections.sort(MvplanInstance.getPrefs().getPrefGases());
			System.out.println(g);
			a = new GasListAdaptor(getApplicationContext(),
					R.layout.gas_list_label, R.id.gas_list_label_text,
					MvplanInstance.getPrefs().getPrefGases());
			list.setAdapter(a);
		}
	};

	private class GasListAdaptor extends ArrayAdapter<Gas> {

		List <Gas> selected;
		public GasListAdaptor(Context context, int resource,
							  int textViewResourceId, List<Gas> objects) {
			super(context, resource, textViewResourceId, objects);
			selected = new ArrayList<Gas>();
			//TOTO save selected somewhere;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View a =  super.getView(position, convertView, parent);

			//handle buttons
			ImageButton b = (ImageButton) a.findViewById(R.id.gas_list_label_edit);
			b.setTag(Integer.valueOf(position));
			b.setOnClickListener(editButtonListener);
			b = (ImageButton) a.findViewById(R.id.gas_list_label_delete);
			b.setTag(Integer.valueOf(position));
			b.setOnClickListener(deleteButtonListener);

			//handle checkbox;
			CheckBox cb = (CheckBox) a.findViewById(R.id.gas_list_label_check_box);
			cb.setOnCheckedChangeListener(checkedListener);
			cb.setTag(Integer.valueOf(position));
			cb.setChecked(getItem(position).getEnable());
			return a;
		}

		android.view.View.OnClickListener editButtonListener = new android.view.View.OnClickListener() {
			public void onClick(View v) {

				Gas g = a.getItem((Integer) v.getTag());
				EditCallback e = new EditCallback((Integer) v.getTag());
				new GasDialog(GasList.this ,g, e).show();
			}
		};

		android.view.View.OnClickListener deleteButtonListener = new android.view.View.OnClickListener() {
			public void onClick(View v) {
				a.remove(a.getItem((Integer) v.getTag()));
			}
		};

		OnCheckedChangeListener checkedListener = new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				int position = (Integer) buttonView.getTag();
				a.getItem(position).setEnable(isChecked);
			}
		};
	}
	private class EditCallback implements GasDialogCallback{
		public EditCallback(int position) {
			this.position=position;
		}
		int position;
		public void notify(Gas g) {
			a.remove(a.getItem(position));
			a.insert(g, position);
		}
	}

	public void reloadAdaptor(){
		a = new GasListAdaptor(this.getApplicationContext(),
				R.layout.gas_list_label, R.id.gas_list_label_text,
				MvplanInstance.getPrefs().getPrefGases());
		list.setAdapter(a);
	}

}