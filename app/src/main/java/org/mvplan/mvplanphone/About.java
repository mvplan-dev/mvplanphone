package org.mvplan.mvplanphone;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.webkit.WebView;

public class About extends mvPlanPhoneActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		
		WebView t = (WebView) findViewById(R.id.about_text);
		
		t.loadData(getString(R.string.about_text), "text/html", null);
		
		
		Button b = (Button) findViewById(R.id.about_close_button);
		b.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}

}