/*
 * SegmentList.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone;

import java.util.List;

import mvplan.main.MvplanInstance;
import mvplan.segments.SegmentAbstract;

import org.mvplan.mvplanphone.gui.SegmentDialog;
import org.mvplan.mvplanphone.gui.SegmentDialogCallback;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SegmentList extends mvPlanPhoneActivity {
	Button add;
	ListView list;
	SegmentListAdaptor a;

	@Override
	protected void onResume() {
		super.onResume();
		a = new SegmentListAdaptor(this.getApplicationContext(),
				R.layout.segment_list_label, R.id.segment_list_label_text,
				MvplanInstance.getPrefs().getPrefSegments());
		list.setAdapter(a);
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.segment_list_view);
		list = (ListView) findViewById(R.id.segment_list_list);
		add = (Button) findViewById(R.id.segment_list_add);
		add.setOnClickListener(addButtonListener);
		a = new SegmentListAdaptor(this.getApplicationContext(),
				R.layout.segment_list_label, R.id.segment_list_label_text,
				MvplanInstance.getPrefs().getPrefSegments());
		list.setAdapter(a);

	}

	android.view.View.OnClickListener addButtonListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			new SegmentDialog(SegmentList.this, addCallback).show();
		}
	};

	SegmentDialogCallback addCallback = new SegmentDialogCallback() {
		public void notify(SegmentAbstract g) {
			MvplanInstance.getPrefs().getPrefSegments().add(g);
			System.out.println(g);
			a = new SegmentListAdaptor(getApplicationContext(),
					R.layout.segment_list_label, R.id.segment_list_label_text,
					MvplanInstance.getPrefs().getPrefSegments());
			list.setAdapter(a);
		}
	};

	private class SegmentListAdaptor extends ArrayAdapter<SegmentAbstract> {

		public SegmentListAdaptor(Context context, int resource,
				int textViewResourceId, List<SegmentAbstract> objects) {
			super(context, resource, textViewResourceId, objects);
			// TOTO save selected somewhere;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View a = super.getView(position, convertView, parent);

			SegmentAbstract item = getItem(position);
			// handle buttons
			ImageButton b = (ImageButton) a
					.findViewById(R.id.segment_list_label_edit);
			b.setTag(new Integer(position));
			b.setOnClickListener(editButtonListener);
			b = (ImageButton) a.findViewById(R.id.segment_list_label_delete);
			b.setTag(new Integer(position));
			b.setOnClickListener(deleteButtonListener);

			// handle checkbox;
			CheckBox cb = (CheckBox) a
					.findViewById(R.id.segment_list_label_check_box);
			cb.setOnCheckedChangeListener(checkedListener);
			cb.setTag(new Integer(position));
			cb.setChecked(item.getEnable());

			TextView text = (TextView) a
					.findViewById(R.id.segment_list_label_text);

			text.setText(MvplanInstance.getMvplan().getResource("mvplan.gui.MainFrame.diveTable.time.text") + " : " + (int)item.getTime() + " min\n " + MvplanInstance.getMvplan().getResource("mvplan.gui.MainFrame.diveTable.depth.text") + " :    "
					+ (int)item.getDepth() + " "
					+ MvplanInstance.getPrefs().getDepthShortString() + "\n Gas:      "
					+ item.getGas() + "\n SetPoint: " + item.getSetpoint());
			return a;
		}

		android.view.View.OnClickListener editButtonListener = new android.view.View.OnClickListener() {
			public void onClick(View v) {

				SegmentAbstract g = a.getItem((Integer) v.getTag());
				EditCallback e = new EditCallback((Integer) v.getTag());
				new SegmentDialog(SegmentList.this, g, new EditCallback(
						(Integer) v.getTag())).show();
			}
		};

		android.view.View.OnClickListener deleteButtonListener = new android.view.View.OnClickListener() {
			public void onClick(View v) {
				a.remove(a.getItem((Integer) v.getTag()));
			}
		};

		OnCheckedChangeListener checkedListener = new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				int position = (Integer) buttonView.getTag();
				a.getItem(position).setEnable(isChecked);
			}
		};
	}

	private class EditCallback implements SegmentDialogCallback {
		public EditCallback(int position) {
			this.position = position;
		}

		int position;

		public void notify(SegmentAbstract g) {
			a.remove(a.getItem(position));
			a.insert(g, position);

		}
	}
	
	public void reloadAdaptor(){
		a = new SegmentListAdaptor(getApplicationContext(),
				R.layout.segment_list_label, R.id.segment_list_label_text,
				MvplanInstance.getPrefs().getPrefSegments());
		list.setAdapter(a);
	}

}
