package org.mvplan.mvplanphone;

public class mvPlanPhoneException extends Exception {
	private static final long serialVersionUID = -1870685529541848828L;
	private String alert;
	public mvPlanPhoneException(String a){
		alert = a;
	}
	public String getAlert(){
		return alert;
	}
}
