/*
 * Init.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone.preferences.migrations;

import org.mvplan.mvplanphone.preferences.SharedPreferencesDAO;
import org.mvplan.mvplanphone.preferences.serialization.PrefsSerialization;

import mvplan.main.MvplanInstance;
import mvplan.prefs.Prefs;

import com.google.gson.Gson;

import android.content.SharedPreferences;

/**
 * The Class Init is responsble for initializing shared preferences to first version.
 */
public class Init implements Migration {
	private static final int INIT_VERSION = 1;
	
	Gson gson = new Gson();
	
	public void migrate(SharedPreferences prefs) {
		Prefs mvPrefs = MvplanInstance.getPrefs();
		PrefsSerialization s = new PrefsSerialization(mvPrefs);
		
		SharedPreferences.Editor editor = prefs.edit();

		editor.putString(SharedPreferencesDAO.PREFS_ID_VERSION, ""+INIT_VERSION);
		editor.putString(SharedPreferencesDAO.PREFS_ID_PREF_GASES, s.serializeGases());
		editor.putString(SharedPreferencesDAO.PREFS_ID_PREF_SEGMENTS,  s.serializeSegments());
		editor.putBoolean(SharedPreferencesDAO.PREFS_ID_CHANGE_GASES, mvPrefs.getOcDeco());

		editor.putString(SharedPreferencesDAO.PREFS_ID_UNITS, ""+mvPrefs.getUnits());
		editor.putString(SharedPreferencesDAO.PREFS_ID_GF_LOW,  ""+(int)((mvPrefs.getGfLow()*100)));
		editor.putString(SharedPreferencesDAO.PREFS_ID_GF_HIGH, ""+(int)((mvPrefs.getGfHigh()*100)));
		editor.putString(SharedPreferencesDAO.PREFS_ID_DIVE_RVM, ""+ (int)mvPrefs.getDiveRMV());
		editor.putString(SharedPreferencesDAO.PREFS_ID_DECO_RVM, ""+ (int)mvPrefs.getDecoRMV());
		editor.putString(SharedPreferencesDAO.PREFS_ID_DESCENT_RATE, ""+ (int)mvPrefs.getDescentRate());
		editor.putString(SharedPreferencesDAO.PREFS_ID_ASCENT_RATE, ""+ (int)mvPrefs.getAscentRate());
		editor.putString(SharedPreferencesDAO.PREFS_ID_LAST_STOP_DEPTH, ""+ (Double)mvPrefs.getLastStopDepth());
		editor.putString(SharedPreferencesDAO.PREFS_ID_ALTITUDE, ""+ (int)mvPrefs.getAltitude());
		editor.putBoolean(SharedPreferencesDAO.PREFS_ID_MULTILEVEL, mvPrefs.getGfMultilevelMode());
		editor.putString(SharedPreferencesDAO.PREFS_ID_OUTPUT_STYLE, ""+mvPrefs.getOutputStyle());

		editor.commit();
	}

}
