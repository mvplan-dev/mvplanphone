/*
 * MVPlanPreferences.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone.gui;

import org.mvplan.mvplanphone.R;
import org.mvplan.mvplanphone.preferences.SharedPreferencesDAO;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.text.InputType;
import android.widget.Toast;

public class MVPlanPreferences extends PreferenceActivity {
	private static final String NAME = "mvPlanSharedPrefs";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.mvprefs);
		
		makeDouble(SharedPreferencesDAO.PREFS_ID_LAST_STOP_DEPTH);
		makeNumber(SharedPreferencesDAO.PREFS_ID_GF_HIGH);
		makeNumber(SharedPreferencesDAO.PREFS_ID_GF_LOW);
		makeNumber(SharedPreferencesDAO.PREFS_ID_DIVE_RVM);
		makeNumber(SharedPreferencesDAO.PREFS_ID_DECO_RVM);
		makeNumber(SharedPreferencesDAO.PREFS_ID_DESCENT_RATE);
		makeNumber(SharedPreferencesDAO.PREFS_ID_ASCENT_RATE);
		makeNumber(SharedPreferencesDAO.PREFS_ID_ALTITUDE);
		
		// Get the custom preference
		Preference customPref = (Preference) findPreference(SharedPreferencesDAO.PREFS_ID_LAST_STOP_DEPTH);
		customPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
						
						SharedPreferences customSharedPreference = getSharedPreferences(
								NAME, Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = customSharedPreference
								.edit();
						try {
							editor.putString(SharedPreferencesDAO.PREFS_ID_LAST_STOP_DEPTH, ""+Double.parseDouble(newValue.toString()));
						} catch (NumberFormatException e) {
							Toast.makeText(getBaseContext(),
									"Could not set new depth :"+newValue+" make sure it is number",
									Toast.LENGTH_LONG).show();
							e.printStackTrace();
						}
						editor.commit();
						
						return true;
					}


				});
	}

	private void makeNumber(String key){
		EditTextPreference prefEditText = (EditTextPreference) findPreference(key);
		prefEditText.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
	}

	private void makeDouble(String key){
		EditTextPreference prefEditText = (EditTextPreference) findPreference(key);
		prefEditText.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
	}

}
