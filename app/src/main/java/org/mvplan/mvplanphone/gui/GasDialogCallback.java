package org.mvplan.mvplanphone.gui;

import mvplan.gas.Gas;

public interface GasDialogCallback {
	public void notify(Gas g);
}
