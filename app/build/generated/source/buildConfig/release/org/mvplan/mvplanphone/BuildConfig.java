/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.mvplan.mvplanphone;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "org.mvplan.mvplanphone";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 914;
  public static final String VERSION_NAME = "0.9.14";
}
