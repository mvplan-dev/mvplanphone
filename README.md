# mvplan for android smartphone

To build this android app you have to:

- install android studio from https://developer.android.com/studio/
- clone this source code : `git clone https://framagit.org/mvplan-dev/mvplanphone.git`
- open project in android studio
- file > project structure (popup, change version and others prop)
- compile / build APK / build AAB

Then make some modifications / enhancements and commit / send your patches !

That's all !